<?php

namespace Bss\Internship\Block;

use phpDocumentor\Reflection\Types\This;

class Show extends \Magento\Framework\View\Element\Template
{
    protected $_internshipFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\Internship\Model\InternshipFactory $internshipFactory
    )
    {
        $this->_internshipFactory = $internshipFactory;
        parent::__construct($context);
    }


    public function getInternship()
    {
        $id = $this->getRequest()->getParams('id');
        $user = $this->_internshipFactory->create()->load($id);

        if ($user->getId() == null) {
            header('HTTP/1.0 404 Not Found');
            exit;
        }
        return $user;
    }
}
