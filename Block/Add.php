<?php
namespace Bss\Internship\Block;

class Add extends \Magento\Framework\View\Element\Template
{
    protected $_internshipFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\Internship\Model\InternshipFactory $internshipFactory
    )
    {
        $this->_internshipFactory = $internshipFactory;
        parent::__construct($context);
    }

}
