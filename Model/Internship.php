<?php
namespace Bss\Internship\Model;

class Internship extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'bss_internship_internship';

    protected $_cacheTag = 'bss_internship_internship';

    protected $_eventPrefix = 'bss_internship_internship';

    protected function _construct()
    {
        $this->_init('Bss\Internship\Model\ResourceModel\Internship');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        return [];
    }
}
