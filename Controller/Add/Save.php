<?php

namespace Bss\Internship\Controller\Add;

use Magento\Framework\App\Action\Context;
use Bss\Internship\Model\InternshipFactory;

class Save extends \Magento\Framework\App\Action\Action
{

    protected $_internshipFactory;

    public function __construct(
        Context $context,
        InternshipFactory $internshipFactory
    )
    {
        $this->_internshipFactory = $internshipFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $internship = $this->_internshipFactory->create();
        $internship->setData($data);
        if ($internship->save()) {
            $this->messageManager->addSuccessMessage(__('You saved the data.'));
        } else {
            $this->messageManager->addErrorMessage(__('Data was not saved.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('internship/index/index');
        return $resultRedirect;
    }
}
