<?php

namespace Bss\Internship\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
    const YOUR_STORE_ID = 1;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $_blockFactory;

    /**
     * UpgradeData constructor
     *
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     */
    public function __construct(
        \Magento\Cms\Model\BlockFactory $blockFactory
    )
    {
        $this->_blockFactory = $blockFactory;
    }

    /**
     * Upgrade data for the module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws \Exception
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        // run the code while upgrading module to version 0.0.2
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            $cmsBlock = $this->_blockFactory->create()->setStoreId(self::YOUR_STORE_ID)->load('test-block', 'identifier');

            $cmsBlockData = [
                'title' => 'Test Block 2',
                'identifier' => 'test-block',
                'is_active' => 1,
                'stores' => [self::YOUR_STORE_ID],
                'content' => "<div class='block'>
                                <h2>My Test Block 2</h2>
                            </div>",
            ];

            if (!$cmsBlock->getId()) {
                $this->_blockFactory->create()->setData($cmsBlockData)->save();
            } else {
                $cmsBlock->setContent($cmsBlockData['content'])->save();
            }
        }
        $setup->endSetup();
    }
}
