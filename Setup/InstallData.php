<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bss\Internship\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install
         */
        $setup->startSetup();

        $data = [
            [
                'id' => null,
                'name' => 'Le Thanh Hai',
                'avatar' => 'http://www.site-magento.org/site1/pub/media/images.1png',
                'dob' => '1992/10/05',
                'description' => 'Happy New Year',
            ],

            [
                'id' => null,
                'name' => 'ABC',
                'avatar' => 'http://www.site-magento.org/site1/pub/media/images.1png',
                'dob' => '1992/10/05',
                'description' => 'Happy New Year',
            ],

            [
                'id' => null,
                'name' => '123',
                'avatar' => 'http://www.site-magento.org/site1/pub/media/images.1png',
                'dob' => '1992/10/05',
                'description' => 'Happy New Year',
            ],

            [
                'id' => null,
                'name' => 'xyz',
                'avatar' => 'http://www.site-magento.org/site1/pub/media/images.1png',
                'dob' => '1992/10/05',
                'description' => 'Happy New Year',
            ],

            [
                'id' => null,
                'name' => 'c0819h2',
                'avatar' => 'http://www.site-magento.org/site1/pub/media/images.1png',
                'dob' => '1992/10/05',
                'description' => 'Happy New Year',
            ]
        ];

        foreach ($data as  $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('internship'), $bind);
        }
        $setup->endSetup();
    }
}
